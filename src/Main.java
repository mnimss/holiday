import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        out.println("Вы женщина или мужчина?");

        out.println("Введите man или woman");

        out.flush();

        String response = in.next();

        try {
            Human human = HumanFactory.createHuman(response);

            out.println(human.provideGift());

            out.flush();
        }
        catch (IllegalArgumentException e){

            out.println("Привет, мой небинарный друг! К сожалению, для тебя подарка не нашлось :(");

            out.flush();
        }
    }
}