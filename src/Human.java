public abstract class Human implements Gift {

    protected String gift;

    @Override
    public String provideGift() {

        return "Держи свой подарок :) \n" + gift;
    }
}