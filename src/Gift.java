public interface Gift {

    String provideGift();
}