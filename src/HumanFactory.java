public final class HumanFactory {

    private HumanFactory() {
    }

    public static Human createHuman(String typeOfHuman){

        switch (typeOfHuman){

            case "man" : return new Man();

            case "woman" : return new Woman();

            default: throw new IllegalArgumentException("There is no such typeOfHuman");
        }
    }
}